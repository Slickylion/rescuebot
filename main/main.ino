/* Arduino
  // Motor A connections
  int enA = 9;
  int in1 = 8;
  int in2 = 7;
  // Motor B connections
  int enB = 3;
  int in3 = 5;
  int in4 = 4;
*/
#include<ESP8266WiFi.h>

const char* ssid = "BenES";
const char* password = "Benalleen001";

// esp
// Motor A connections
#define enA 1
#define in1 3
#define in2 15
// Motor B connections
#define enB 14
#define in3 13
#define in4 12
#define echoPin 4 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 5 //attach pin D3 Arduino to pin Trig of HC-SR04
#define irRight 2
#define irLeft 10
#define irFront 5
#define reed 4



long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
bool autonomous;
int driveStatus; // 0 = stop, 1 = forward 2 = left, 3 = right

WiFiServer server(80);
WiFiClient client;
bool sent;
const char* javascript =   "<script>function forward() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=forward', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}"
                           "function autoOn() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=autoOn', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}"
                           "function autoOff() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=autoOff', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}"
                           "function left() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=left', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}"
                           "function right() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=right', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}"
                           "function stop() {"
                           "var http = new XMLHttpRequest();"
                           "http.open(\"GET\", '/control/control=stop', true);"
                           "http.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");"
                           "http.send(200);"
                           "}</script>";

void sendControlHTML() {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<table>");
  client.println("<tr>");
  client.println("<td></td>");
  client.println("<td>");
  client.println("<button onmousedown=\"forward()\" ontouchstart=\"forward()\" ontouchend=\"stop()\" onmouseup=\"stop()\"><span style=\"-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none; font-size:xxx-large\">Forward</span></button>");
  client.println("</td>");
  client.println("/<tr>");
  client.println("<td>");
  client.println("<button onmousedown=\"left()\" ontouchstart=\"left()\" ontouchend=\"stop()\" onmouseup=\"stop()\"><span style=\"-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none; font-size:xxx-large\">Left</span></button>");
  client.println("</td>");
  client.println("<td></td>");
  client.println("<td>");
  client.println("<button onmousedown=\"right()\" ontouchstart=\"right()\" ontouchend=\"stop()\" onmouseup=\"stop()\"><span style=\"-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none; font-size:xxx-large\">Right</span></button>");
  client.println("</td>");
  client.println("<td>");
  client.println("<button onclick=\"autoOn()\" ontouchstart=\"autoOn()\" style=\"font-size:xxx-large\">Auto on</button>");
  client.println("<button onclick=\"autoOff()\" ontouchstart=\"autoOff()\" style=\"font-size:xxx-large\">Auto off</button>");
  client.println("</td>");
  client.println("</tr>");
  client.println("</table>");



  if (sent) {
    client.println("VERY GOOD");
  }
  client.println("<html>");
  // javascript
  client.println(javascript);
  client.println("");

}

void handleClient() {
  client = server.available();
  if (client)
  {
    String request = client.readStringUntil('\r');
    Serial.println("Client available");

    if (request.indexOf("/control/control=forward") != -1) {
      client.println("HTTP/1.1 200 OK");
      Serial.println("forward!");
      forward();
    }
    else if (request.indexOf("/control/control=stop") != -1) {
      client.println("HTTP/1.1 200 OK");
      Serial.println("stay");
      motorStop();
    }
    else if (request.indexOf("/control/control=right") != -1) {
      client.println("HTTP/1.1 200 OK");
      Serial.println("stay");
      right();
    }
    else if (request.indexOf("/control/control=left") != -1) {
      client.println("HTTP/1.1 200 OK");
      Serial.println("stay");
      left();
    }
    else if (request.indexOf("/control/control=autoOn") != -1) {
      client.println("HTTP/1.1 200 OK");
      autonomous = true;
    }
    else if (request.indexOf("/control/control=autoOff") != -1) {
      client.println("HTTP/1.1 200 OK");
      autonomous = false;
      motorStop();
    }
    else {
      sendControlHTML();
    }
    client.flush();
  }

}

//client.stop();


void setup() {
  bool autonomous = true;
  sent = false;
  // Set all the motor control pins to outputs

  //pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  //pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
  Serial.begin(115200);
  pinMode(irLeft, INPUT);
  pinMode(irRight, INPUT);
  pinMode(irFront, INPUT);
  pinMode(reed, INPUT);
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  pinMode(16, OUTPUT);

  //motorSpeed(512);

  //Serial.println("Starting wifi");
  connectWifi();
  server.begin(80);

}


void loop() {
  if (autonomous) {
    drive();
  }
  handleClient();
}

void connectWifi() {
  Serial.println("Trying to connect");
  WiFi.hostname("BenESP");
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

int ultrasoneRead() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  return (int)distance;
}

void backward() {
  motorSpeed(1024);

  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void left() {
  motorSpeed(700);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void right() {
  motorSpeed(700);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void motorStop() {
  motorSpeed(0);
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}
void forward() {
  analogWrite(enA, 1024);
  analogWrite(enB, 1024);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void randTurn(int ms) {
  int r = random(1, 51);
  if (r > 25) {
    right();
    delay(ms);
  }
  else {
    left();
    delay(ms);
  }
}
void drive() {
  if (digitalRead(irFront) == LOW) {
    backward();
    delay(500);
    randTurn(300);
    return;
  }
  if (digitalRead(irRight) == HIGH && digitalRead(irLeft) == HIGH) {
    backward();
    delay(600);
    int r = random(1, 51);
    if (r > 25) {
      right();
      delay(300);
    }
    else {
      left();
      delay(300);
    }

  }
  else if (digitalRead(irRight) == 1) {
    left();
    delay(150);
  }
  else if (digitalRead(irLeft) == 1) {
    right();
    delay(150);
  }
  else {
    forward();
  }

}



// This function lets you control speed of the motors
void motorSpeed(int s) {
  // Turn on motors
  analogWrite(enA, s);
  analogWrite(enB, s);
}
